FROM python:3.8.3-alpine

WORKDIR /usr/src/app

COPY trying.py .

COPY mymodule.py .

COPY requirements.txt .
RUN \
 apk add --no-cache postgresql-libs && \
 apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev && \
 python3 -m pip install -r requirements.txt --no-cache-dir && \
 apk --purge del .build-deps

CMD ["python", "./trying.py"]